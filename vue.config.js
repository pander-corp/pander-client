// module.exports = {
//     css: {
//       loaderOptions: {
//         sass: {
//           prependData: `@import "@/assets/scss/__variables.scss";`
//         }
//       }
//     }
//   };

// const path = require('path')
module.exports = {
  // chainWebpack: config => {
  //   const svgRule = config.module.rule('svg')

  //   // clear all existing loaders.
  //   // if you don't do this, the loader below will be appended to
  //   // existing loaders of the rule.
  //   svgRule.uses.clear()

  //   // add replacement loader(s)
  //   svgRule
  //     .use('vue-svg-loader')
  //     .loader('vue-svg-loader')
  // },
  pluginOptions: {
    // svg: {
    //   inline: {}, // Pass options to vue-svg-loader
    //   data: {}, // Pass options to url-loader
    //   sprite: {}, // Pass options to svg-sprite-loader
    //   external: {} // Pass options to file-loader
    // },
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: [
        'F:\\Projects\\pander_client\\src\\assets\\scss\\*.scss',
        'F:\\Projects\\pander_client\\src\\assets\\scss\\standard_components\\*.scss'
      ]
    }
  }
}
